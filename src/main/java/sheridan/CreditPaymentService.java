/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

public class CreditPaymentService extends PaymentService {

    public String processPayment(double amount) {
        String result = "Processing credit payment of $" + amount;
        System.out.println(result);
        return result;
    }

}
