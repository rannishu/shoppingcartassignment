package sheridan;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class ShoppingCartTest {

    @Test
    public void testDebitCardPayment() {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentService debitService = factory.getPaymentService(PaymentServiceType.DEBIT);
        Cart cart = new Cart();
        cart.addProduct(new Product("shirt", 50));
        cart.addProduct(new Product("pants", 60));
        cart.setPaymentService(debitService);
        assertEquals(cart.payCart(), "Processing debit payment of $" + cart.getTotal());
    }

    @Test
    public void testCreditCardPayment() {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentService creditService = factory.getPaymentService(PaymentServiceType.CREDIT);
        Cart cart = new Cart();
        cart.addProduct(new Product("shirt", 50));
        cart.addProduct(new Product("pants", 60));
        cart.setPaymentService(creditService);
        assertEquals(cart.payCart(), "Processing credit payment of $" + cart.getTotal());
    }

}
